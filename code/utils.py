import numpy as np
import random as rn
import numpy
from keras.layers import *
import keras
from keras.engine.topology import Layer
from keras import initializers
import warnings
import DataRep
from keras import initializers
from keras.models import Model
from keras.layers import Input,Multiply
from keras.layers import Lambda
from keras.layers import Reshape
from keras.optimizers import Adam,RMSprop,Adagrad,Adadelta
from keras.layers import Conv1D, Conv2D
from keras.layers import Activation
from keras.layers.noise import *
from keras.layers import GlobalAveragePooling2D, GlobalAveragePooling1D, MaxPooling2D,MaxPooling1D,AveragePooling2D,AveragePooling1D
from keras.layers import BatchNormalization
from keras.layers import Dense
from keras.regularizers import l2
from keras.layers import Concatenate, concatenate
from keras.layers import Add
from keras.layers import Multiply
from keras.layers.embeddings import Embedding
from keras import backend as K
from keras.layers import MaxPooling1D,Input,Reshape,Flatten,GlobalMaxPooling1D
from keras.initializers import Constant
from keras import initializers
from attention import *
seed = 2020
L2_value=0.0001
def Gated_conv1d(seq,kernel_size):
    dim = K.int_shape(seq)[-1]
    h = Conv1D(dim*2, kernel_size, padding='same', kernel_initializer = initializers.lecun_normal(seed = seed),kernel_regularizer=l2(L2_value))(seq)
    def _gate(x):
        s, h = x
        g, h = h[:, :, :dim], h[:, :, dim:]
        g = K.sigmoid(g)
        h = K.selu(h)
        return s + g * h
    seq = Lambda(_gate)([seq, h])
    return seq

def GCRN(filters, ks, t_input, dropout_rate):
    ini_featuresb0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input) 
    ini_featuresb1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input)  
    ini_featuresb2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2]) 
    g_features = Gated_conv1d(ini_featuresb, ks)
    return g_features 
# TISNet1
def build_TISNet(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix, l_r, ks, units):
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    #1
    g_features1 = GCRN(filters, ks, embed_features, dropout_rate)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    g_features2 = GCRN(filters, ks, g_features1, dropout_rate)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    g_features3 = GCRN(filters, ks, g_features2, dropout_rate)
    g_features3 = Dropout(dropout_rate)(g_features3)
    #4
    g_features4 = GCRN(filters, ks, g_features3, dropout_rate)
    g_features4 = Dropout(dropout_rate)(g_features4)
    #5
    g_features5 = GCRN(filters, ks, g_features4, dropout_rate)
    g_features5 = Dropout(dropout_rate)(g_features5)

    #6
    g_features6 = GCRN(filters, ks, g_features5, dropout_rate)
    g_features6 = Dropout(dropout_rate)(g_features6)
    
    # #7    
    # g_features7 = GCRN(filters, ks, g_features6, dropout_rate)
    # g_features7 = Dropout(dropout_rate)(g_features7) 

    # #8    
    # g_features8 = GCRN(filters, ks, g_features7, dropout_rate)
    # g_features8 = Dropout(dropout_rate)(g_features8)
    # #9 
    # g_features9 = GCRN(filters, ks, g_features8, dropout_rate)
    # g_features9 = Dropout(dropout_rate)(g_features9) 
    
    g_features = GCRN(filters, ks, g_features6, dropout_rate)

    s_features = Attention(8,16)([embed_features, embed_features, embed_features])

    features = keras.layers.concatenate([g_features, s_features])
    features = Dropout(dropout_rate)(features)
    all_features = Flatten()(features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    TISNet = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

    TISNet.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    TISNet.summary()
    TISNet.get_config()
    return TISNet
###############################################################################################################
def Gated_conv1d_linear(seq,kernel_size):
    dim = K.int_shape(seq)[-1]
    h = Conv1D(dim*2, kernel_size, padding='same',kernel_regularizer=l2(L2_value))(seq)
    def _gate(x):
        s, h = x
        g, h = h[:, :, :dim], h[:, :, dim:]
        g = K.sigmoid(g)
        h = K.linear(h)
        return s + g * h
    seq = Lambda(_gate)([seq, h])
    return seq
def GCN_linear(filters, ks, t_input, dropout_rate):
    ini_featuresb0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input) 
    ini_featuresb1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input)  
    ini_featuresb2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2]) 
    g_features = Gated_conv1d_linear(ini_featuresb, ks)
    return g_features 
# TISNet1
def build_TISNet_linear(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix, l_r, ks, units):
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    #1
    g_features1 = GCN_linear(filters, ks, embed_features, dropout_rate)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    g_features2 = GCN_linear(filters, ks, g_features1, dropout_rate)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    g_features3 = GCN_linear(filters, ks, g_features2, dropout_rate)
    g_features3 = Dropout(dropout_rate)(g_features3)
    #4
    g_features4 = GCN_linear(filters, ks, g_features3, dropout_rate)
    g_features4 = Dropout(dropout_rate)(g_features4)
    #5
    g_features5 = GCN_linear(filters, ks, g_features4, dropout_rate)
    g_features5 = Dropout(dropout_rate)(g_features5)

    #6
    g_features6 = GCN_linear(filters, ks, g_features5, dropout_rate)
    g_features6 = Dropout(dropout_rate)(g_features6)
       
    g_features = GCN_linear(filters, ks, g_features6, dropout_rate)
    s_features = Attention(8,16)([embed_features, embed_features, embed_features])

    features = keras.layers.concatenate([g_features, s_features])
    features = Dropout(dropout_rate)(features)
    all_features = Flatten()(features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    TISNet_linear = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    TISNet_linear.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    TISNet_linear.summary()
    TISNet_linear.get_config()
    return TISNet_linear
###########################################################################################################

def Gated_conv1d_relu(seq,kernel_size):
    dim = K.int_shape(seq)[-1]
    h = Conv1D(dim*2, kernel_size, padding='same',kernel_regularizer=l2(L2_value))(seq)
    def _gate(x):
        s, h = x
        g, h = h[:, :, :dim], h[:, :, dim:]
        g = K.sigmoid(g)
        h = K.relu(h)
        return s + g * h
    seq = Lambda(_gate)([seq, h])
    return seq
def GCN_relu(filters, ks, t_input, dropout_rate):
    ini_featuresb0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input) 
    ini_featuresb1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input)  
    ini_featuresb2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2]) 
    g_features = Gated_conv1d_relu(ini_featuresb, ks)
    return g_features 
# TISNet1
def build_TISNet_relu(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix, l_r, ks, units):
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    #1
    g_features1 = GCN_relu(filters, ks, embed_features, dropout_rate)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    g_features2 = GCN_relu(filters, ks, g_features1, dropout_rate)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    g_features3 = GCN_relu(filters, ks, g_features2, dropout_rate)
    g_features3 = Dropout(dropout_rate)(g_features3)
    #4
    g_features4 = GCN_relu(filters, ks, g_features3, dropout_rate)
    g_features4 = Dropout(dropout_rate)(g_features4)
    #5
    g_features5 = GCN_relu(filters, ks, g_features4, dropout_rate)
    g_features5 = Dropout(dropout_rate)(g_features5)

    #6
    g_features6 = GCN_relu(filters, ks, g_features5, dropout_rate)
    g_features6 = Dropout(dropout_rate)(g_features6)
       
    g_features = GCN_relu(filters, ks, g_features6, dropout_rate)
    s_features = Attention(8,16)([embed_features, embed_features, embed_features])

    features = keras.layers.concatenate([g_features, s_features])
    features = Dropout(dropout_rate)(features)
    all_features = Flatten()(features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    TISNet_relu = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    TISNet_relu.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    TISNet_relu.summary()
    TISNet_relu.get_config()
    return TISNet_relu
####################################################################################################
def Gated_conv1d_elu(seq,kernel_size):
    dim = K.int_shape(seq)[-1]
    h = Conv1D(dim*2, kernel_size, padding='same',kernel_regularizer=l2(L2_value))(seq)
    def _gate(x):
        s, h = x
        g, h = h[:, :, :dim], h[:, :, dim:]
        g = K.sigmoid(g)
        h = K.elu(h)
        return s + g * h
    seq = Lambda(_gate)([seq, h])
    return seq
def GCN_elu(filters, ks, t_input, dropout_rate):
    ini_featuresb0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input) 
    ini_featuresb1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input)  
    ini_featuresb2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(t_input) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2]) 
    g_features = Gated_conv1d_elu(ini_featuresb, ks)
    return g_features 
# TISNet1
def build_TISNet_elu(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix, l_r, ks, units):
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    #1
    g_features1 = GCN_elu(filters, ks, embed_features, dropout_rate)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    g_features2 = GCN_elu(filters, ks, g_features1, dropout_rate)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    g_features3 = GCN_elu(filters, ks, g_features2, dropout_rate)
    g_features3 = Dropout(dropout_rate)(g_features3)
    #4
    g_features4 = GCN_elu(filters, ks, g_features3, dropout_rate)
    g_features4 = Dropout(dropout_rate)(g_features4)
    #5
    g_features5 = GCN_elu(filters, ks, g_features4, dropout_rate)
    g_features5 = Dropout(dropout_rate)(g_features5)

    #6
    g_features6 = GCN_elu(filters, ks, g_features5, dropout_rate)
    g_features6 = Dropout(dropout_rate)(g_features6)
       
    g_features = GCN_elu(filters, ks, g_features6, dropout_rate)
    s_features = Attention(8,16)([embed_features, embed_features, embed_features])

    features = keras.layers.concatenate([g_features, s_features])
    features = Dropout(dropout_rate)(features)
    all_features = Flatten()(features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    TISNet_elu = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    TISNet_elu.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    TISNet_elu.summary()
    TISNet_elu.get_config()
    return TISNet_elu
################################################################################################################
#TISNet2
def build_GCRNA(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix, l_r, ks, units):
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    #1
    g_features1 = GCRN(filters, ks, embed_features, dropout_rate)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    g_features2 = GCRN(filters, ks, g_features1, dropout_rate)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    g_features3 = GCRN(filters, ks, g_features2, dropout_rate)
    g_features3 = Dropout(dropout_rate)(g_features3)
    #4
    g_features4 = GCRN(filters, ks, g_features3, dropout_rate)
    g_features4 = Dropout(dropout_rate)(g_features4)
    #5
    g_features5 = GCRN(filters, ks, g_features4, dropout_rate)
    g_features5 = Dropout(dropout_rate)(g_features5)
    #6
    g_features6 = GCRN(filters, ks, g_features5, dropout_rate)
    g_features6 = Dropout(dropout_rate)(g_features6)

    g_features = GCRN(filters, ks, g_features6, dropout_rate)
    s_features = Attention(8,16)([g_features, g_features, g_features])
    features = Dropout(dropout_rate)(s_features)
    all_features = Flatten()(features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    GCRNA = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    GCRNA.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    GCRNA.summary()
    GCRNA.get_config()
    return GCRNA
#TISNet2
def build_AGCRN(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix, l_r, ks, units):
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    e_features = Attention(8,16)([embed_features, embed_features, embed_features])
    #1
    g_features1 = GCRN(filters, ks, e_features, dropout_rate)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    g_features2 = GCRN(filters, ks, g_features1, dropout_rate)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    g_features3 = GCRN(filters, ks, g_features2, dropout_rate)
    g_features3 = Dropout(dropout_rate)(g_features3)
    #4
    g_features4 = GCRN(filters, ks, g_features3, dropout_rate)
    g_features4 = Dropout(dropout_rate)(g_features4)
    #5
    g_features5 = GCRN(filters, ks, g_features4, dropout_rate)
    g_features5 = Dropout(dropout_rate)(g_features5)
    #6
    g_features6 = GCRN(filters, ks, g_features5, dropout_rate)
    g_features6 = Dropout(dropout_rate)(g_features6)

    g_features = GCRN(filters, ks, g_features6, dropout_rate)
    features = Dropout(dropout_rate)(g_features)
    all_features = Flatten()(features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    AGCRN = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    AGCRN.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    AGCRN.summary()
    AGCRN.get_config()
    return AGCRN

def build_onehotTISNet(args, word_index, nb_classes,dropout_rate,filters,embedding_matrix,l_r,ks,units):
    print('<<<<<<<<<<<<<<<one hot vectors>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        len(word_index),
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    #1
    g_features1 = GCRN(filters, ks, embed_features, dropout_rate)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    g_features2 = GCRN(filters, ks, g_features1, dropout_rate)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    g_features3 = GCRN(filters, ks, g_features2, dropout_rate)
    g_features3 = Dropout(dropout_rate)(g_features3)
    #4
    g_features4 = GCRN(filters, ks, g_features3, dropout_rate)
    g_features4 = Dropout(dropout_rate)(g_features4)
    #5
    g_features5 = GCRN(filters, ks, g_features4, dropout_rate)
    g_features5 = Dropout(dropout_rate)(g_features5)
    #6
    g_features6 = GCRN(filters, ks, g_features5, dropout_rate)
    g_features6 = Dropout(dropout_rate)(g_features6)    
    g_features = GCRN(filters, ks, g_features6, dropout_rate)
    s_features = Attention(8,16)([embed_features, embed_features, embed_features])
    features = keras.layers.concatenate([g_features, s_features])
    features = Dropout(dropout_rate)(features)
    all_features = Flatten()(features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    onehotTISNet = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    onehotTISNet.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    onehotTISNet.summary()
    onehotTISNet.get_config()
    return onehotTISNet

def build_randTISNet(args, word_index, nb_classes,dropout_rate,filters,l_r,ks,units):
    print('<<<<<<<<<<<<<<<rand vectors>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        input_length=args.sequence_length,
                        trainable=args.trainable)

    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    #1
    g_features1 = GCRN(filters, ks, embed_features, dropout_rate)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    g_features2 = GCRN(filters, ks, g_features1, dropout_rate)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    g_features3 = GCRN(filters, ks, g_features2, dropout_rate)
    g_features3 = Dropout(dropout_rate)(g_features3)
    #4
    g_features4 = GCRN(filters, ks, g_features3, dropout_rate)
    g_features4 = Dropout(dropout_rate)(g_features4)
    #5
    g_features5 = GCRN(filters, ks, g_features4, dropout_rate)
    g_features5 = Dropout(dropout_rate)(g_features5)
    #6
    g_features6 = GCRN(filters, ks, g_features5, dropout_rate)
    g_features6 = Dropout(dropout_rate)(g_features6)
    g_features = GCRN(filters, ks, g_features6, dropout_rate)
    s_features = Attention(8,16)([embed_features, embed_features, embed_features])
    features = keras.layers.concatenate([g_features, s_features])
    features = Dropout(dropout_rate)(features)
    all_features = Flatten()(features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    randTISNet = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    randTISNet.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    randTISNet.summary()
    randTISNet.get_config()
    return randTISNet
